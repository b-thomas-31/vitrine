<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(name="front_")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route(path="/", name="index")
     */
    public function index()
    {
        return $this->render('pages/homepage.html.twig');
    }
}